#!/usr/bin/python

import sys, getopt

def getTeamNameFromScoreArray(scoreArray):
    nameComponents = []
    for component in scoreArray:
        if not component.isdigit():
            nameComponents.append(component)
    if nameComponents[0] == ' ':
        return ' '.join(slice(1, nameComponents))
    elif nameComponents[-1] == ' ':
        nameComponents.pop()
        return ' '.join( nameComponents)
    else: 
        return ' '.join(nameComponents)

def getWinner(match):
    if match["team1"]["goals"] > match["team2"]["goals"]:
        return match["team1"]
    elif match["team2"]["goals"] > match["team1"]["goals"]:
        return match["team2"]
    else: return None

def checkIsDraw(match):
    return match["team1"]["goals"] == match["team2"]["goals"]

def toObject(matchArr):
    match = {
        "team1": {
            "name": getTeamNameFromScoreArray(matchArr[0].split()).capitalize(),
            "goals": matchArr[0].split(' ').pop().replace('\n','')
        },
        "team2": {
            "name": getTeamNameFromScoreArray(matchArr[1].split()).capitalize(),
            "goals": matchArr[1].split(' ')[-1].replace('\n','')
        }
    }
    return match

def makeUnique(list): 
    unique_list = [] 
    for x in list: 
        if x not in unique_list: 
            unique_list.append(x) 
    return unique_list

def getTeams(lines):
    teams = []
    for match in lines:
        matchObj = toObject(match.split(','))
        teams.append(matchObj["team1"])
        teams.append(matchObj["team2"])
    return makeUnique(teams)   

def getWins(lines):
    winners = []
    for match in lines:
        winner = getWinner(toObject(match.split(',')))
        if winner != None:
            winners.append(winner["name"])
    return winners

def getDraws(lines):
    drawers = []
    for match in lines:
        matchObj = toObject(match.split(','))
        isDraw = checkIsDraw(matchObj)
        if isDraw:
            drawers.append(matchObj["team1"]["name"])
            drawers.append(matchObj["team2"]["name"])
    return drawers

def getPointsMap(teams, wins, draws):
    points = {}
    for team in wins:
        if team not in points:
            points[team] = 3
        elif team in points:
            points[team] = points[team] + 3
    for team in draws:
        if team not in points:
            points[team] = 1
        elif team in points:
            points[team] = points[team] + 1
    for team in teams:
        if team["name"] not in points:
            points[team["name"]] = 0
    return points
    
def getRank(team, list):
    teamsWithHigherScore = []
    for i in list:
        if i[1] > team[1]:
            teamsWithHigherScore.append(i)
    return len(teamsWithHigherScore) + 1 

def remove(item, list):
    i=0 
    length = len(list) 
    while(i<length):
        if(list[i]==item):
            list.remove (list[i])
            length = length -1  
            continue
        i = i+1
    return list

def sortByRank(teams):
    for team in teams:
        teamsWithEqualPoints = []
        teamsWithEqualPoints.append(team)
        for t in teams:
            if t["name"] != team["name"]:
                if team["rank"] == t["rank"]:
                    teamsWithEqualPoints.append(t)
        if len(teamsWithEqualPoints) > 1:
            firstSliceIndex = teams.index(teamsWithEqualPoints[0])
            lastSliceIndex = teams.index(teamsWithEqualPoints[-1])+1
            sortedTeamsWithEqualPoints = sorted(teamsWithEqualPoints, key=lambda x: x["name"])
            firstSlice = teams[slice(0, firstSliceIndex)]
            lastSlice = teams[slice(lastSliceIndex, len(teams))]
            for i in sortedTeamsWithEqualPoints:
                remove(i, lastSlice)
            teams = firstSlice + sortedTeamsWithEqualPoints + lastSlice
    return teams

def teamWithRankExistsInList (team, list):
    for t in list:
        if t["rank"] == team["rank"] and t["name"] != team["name"]:
            return True
    return False

def getRankDuplcatesBeforeIndex(rank, index, list):  
    duplicates = []
    for team in list[slice(0, index)]:
        if teamWithRankExistsInList(team, list[slice(0, index)]) == True:
            duplicates.append(team)
    return duplicates

def getTeamRankTwins(t, list):
    twins = []
    for team in list:
        if team["rank"] == t["rank"]:
            twins.append(team)
    return twins

def getTable(lines): 
    wins = getWins(lines)
    draws = getDraws(lines)
    teams = getTeams(lines)
    points = getPointsMap(teams, wins, draws)
    sortedPoints = sorted(points.items(), key = lambda point:(point[1], point[0]), reverse = True)
    pointsWithRanks = []
    for team in sortedPoints:
        rank = getRank(team, sortedPoints)
        teamObj = {
            "name": team[0],
            "points": team[1],
            "rank": rank
        }
        pointsWithRanks.append(teamObj)
    
    sortedPointsWithRanks = sortByRank(pointsWithRanks)
    for team in sortedPointsWithRanks:
        duplicatesOccuringBeforeTeam = getRankDuplcatesBeforeIndex(team["rank"],  sortedPointsWithRanks.index(team),  sortedPointsWithRanks)
        twinRanks = getTeamRankTwins(team, sortedPointsWithRanks )
        duplicatedRanks  = []
        for td in duplicatesOccuringBeforeTeam:
             duplicatedRanks.append(td["rank"])
        duplicatedRanks = makeUnique(duplicatedRanks)
        for tdr in duplicatedRanks:
            if tdr == team["rank"]:
                duplicatedRanks = duplicatedRanks.remove(tdr)
        if duplicatedRanks != None:
            if len(duplicatedRanks) !=0:
                for t in sortedPointsWithRanks:
                    if t in twinRanks and "twinAdjustmentOccured" not in t:
                        t["rank"] = t["rank"] - len(duplicatedRanks)
                        t["twinAdjustmentOccured"] = True
    for t in sortedPointsWithRanks :
        print str(t["rank"])+". "+t["name"]+" "+str(t["points"])+"pts"

def main(argv):
    arg = getopt.getopt(argv,"hi:o:",["ifile=","ofile="])
    inputfile = open(arg[1][0], 'r');
    table = getTable(inputfile.readlines())
    sys.exit(2)

if __name__ == "__main__":
   main(sys.argv[1:])