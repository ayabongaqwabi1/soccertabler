## The following is a commandline application that is used to calculate the ranking table of a soccer league

> Pre-requites : Python Environment & Git
> To find out how you can install Python on your machine please visit [https://www.python.org/](https://www.python.org/)

> To find out how you can install and use Git on your machine you can visit  this [friendly tutorial](https://www.atlassian.com/git/tutorials/install-git)

#### How to run

1. Open a terminal
2. Clone this repo
> `git clone https://gitlab.com/ayabongaqwabi1/soccertabler.git`
3. Change your terminal directory to the folder you have just cloned
> `cd soccertabler`
4. Run `python soccer-tabler.py [path/to/scoresfile.txt]`